# Strategy Pattern

Tenemos diferentes comisiones que se pagan según un monto de venta. Si el monto de venta es mayor se paga mayor comisión.

Definimos la interfaz Strategy de la cual crearemos estrategias puntuales para cada caso.

A su vez tenemos la implementación para un pago de una comisión full, otra normal y una regular.

El método getStrategy decidirá la estrategia a utilizar según el monto. Luego se establecerá esa estrategia en el contexto y se usará para calcular la comisión resultante.

- Decidimos la estrategia que corresponde usar según algún input.
- Definimos la estrategia en el contexto.
- Luego usamos el contexto para determinar el resultado.

Descarga de código: [StrategyPattern GitLab](https://gitlab.com/gonzaloEpiData/strategypattern)