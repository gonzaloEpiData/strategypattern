package com.pattern;

import com.pattern.context.Context;
import com.pattern.service.CommissionStrategy;
import com.pattern.service.impl.FullCommission;
import com.pattern.service.impl.NormalCommission;
import com.pattern.service.impl.RegularCommission;

public class Main {

    public static void main(String[] args) {
        CommissionStrategy commissionStrategy = getStrategy(1000d);
        Context context = new Context(commissionStrategy);
        System.out.println("Commission for 1000 $: " + context.executeStrategy(1000d));

        commissionStrategy = getStrategy(500d);
        context = new Context(commissionStrategy);
        System.out.println("Commission for 500 $: " + context.executeStrategy(500d));

        commissionStrategy = getStrategy(100d);
        context = new Context(commissionStrategy);
        System.out.println("Commission for 100 $: " + context.executeStrategy(100d));
    }

    private static CommissionStrategy getStrategy(double amount) {
        if (amount >= 1000d) {
            return new FullCommission();
        } else if (amount >= 500d && amount <= 999d) {
            return new NormalCommission();
        } else {
            return new RegularCommission();
        }
    }
}
