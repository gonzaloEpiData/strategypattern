package com.pattern.service;

public interface CommissionStrategy {
    double applyCommission(double amount);
}
