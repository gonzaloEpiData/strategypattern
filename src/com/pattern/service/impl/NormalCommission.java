package com.pattern.service.impl;

import com.pattern.service.CommissionStrategy;

public class NormalCommission implements CommissionStrategy {

    @Override
    public double applyCommission(double amount) {
        return amount * 0.30;
    }
}
